export class ExtendableError {

    public message: string;
    public status: number;
    public info: string;
  
    constructor(message: string, status: number, info: string) {
        this.message = message;
        this.status = status;
        this.info = info;
    }
}
  
class APIError extends ExtendableError {

    constructor(message, status = 500, info) {
        super(message, status, info);
        if (this.status !== 400 && this.status !== 401 && this.status !== 404) {
            console.error(this);
        }
    }
}

export default APIError;