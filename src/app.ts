import * as express from 'express';
import * as cors from 'cors';
import * as morgan from 'morgan';
import * as helmet from 'helmet';
import * as methodOverride from 'method-override';
import * as path from 'path';
import routes from './api';

import { getErrorResponse, getResponse } from './api/mainModels';

import APIError from './services/APIError';

class Server {
    public app = express();

    constructor() {
        this.config();
        this.routes();
    };
    private config() {
        /** Enabling cross-origin resource sharing */
        this.app.use(cors({ origin: true }));
        /** Logging api calls */
        this.app.use(morgan('dev'));
    
        /** Enabling middleware that parses json */
        this.app.use(express.json({ limit: '5mb' }), (err, req: express.Request, res: express.Response, next: express.NextFunction) => {
          if (err && err.status === 400) return res.status(400).send(err.type);
        });
        /** Enabling method-override */
        this.app.use(methodOverride());
        /** Opening media folder */
        this.app.use('/', express.static(path.join(__dirname, 'dist')));
    };

    private routes() {

        this.app.use('/', routes);
    
        this.app.use((err, req: express.Request, res: express.Response, next: express.NextFunction) => {
          if (!(err instanceof APIError)) {
            new APIError(err, 500, 'Unknown error');
          }
          res.status(500).send(getErrorResponse());
        });
    
        this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (!req.path.includes('/')) res.sendFile(path.join(__dirname, 'flatbed_dist/dist/index.html'));
            else res.status(404).send({
                success: false,
                message: 'API not found'
            })
        });
    }
}

export default new Server().app;