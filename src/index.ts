import * as http from 'http';

import app from './app';
import mainConfig from './env';

let server;
const runClusters = () => {
  if (mainConfig.ENABLE_CLUSTERS) {
    // listen on port
    server = http.createServer(app).listen(mainConfig.PORT, () => {
        console.log(`Server ${process.pid} started on port ` + mainConfig.PORT + ` in ${mainConfig.NODE_ENV} mode`);
    });
  } else server = http.createServer(app).listen(mainConfig.PORT, () => {
    console.log(`Server ${process.pid} started on port ` + mainConfig.PORT + ` in ${mainConfig.NODE_ENV} mode`);
  });

};

runClusters();

export default server;