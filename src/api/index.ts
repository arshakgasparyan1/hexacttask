import * as express from 'express';
import ScraperRoutes from './scraper';

class Routes {

    public router = express.Router();

    constructor() {
        this.routes();
    };

    private routes = () => {
        this.router.use('', ScraperRoutes);
    };
}

export default new Routes().router;