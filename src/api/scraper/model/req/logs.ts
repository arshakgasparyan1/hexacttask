
class Model {
    constructor(x: string[]) {
        this.website = x;
    }
    public website?: string[];
};

export class RequestModel  {
    constructor(data: Model[]) {
        this.data = data.map(x => new Model(x.website || []));
    }
    
    public data: Model[];
};
