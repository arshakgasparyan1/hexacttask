export class ResponseModel  {

    constructor(data: ResponseModel) {
        this._website = data._website
        this._link = data._link
        this._statusCode = data._statusCode
    }
    public _website?: string[];
    public _link: string[];
    public _statusCode: number[];
};