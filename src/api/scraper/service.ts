import axios from "axios";
import mainConfig from "../../env";
import { IResponseModel, getResponse } from "../mainModels";
import { RequestModel } from "./model/req/logs";
import { ResponseModel } from "./model/res/response";

class ScraperService {

    public getInfo = async (body: RequestModel): Promise<IResponseModel<{ status: boolean, message: string, data?: { responseData: ResponseModel[], links: string[]} }>> => {
        this.sendDataToScraper(body);
        return getResponse(true, "please wait and see logs")
    }

    public sendDataToScraper = async (body: RequestModel): Promise<IResponseModel<{ status: boolean, message: string, data?: ResponseModel[] }>> => {
        let result: ResponseModel[] = []
        for (const item of body.data) {
            for (const url of item.website) {
                const {data} = await this.getResponsesScraper(url);                
                result = result.concat(data.responseData)
            }
        }
        console.log(result);
        
        return getResponse(true, "please wait and see logs")
    }

    public getResponsesScraper = async (url: string): Promise<{ status: boolean, message: string, data?: { responseData: ResponseModel[], links: string[]}}> => {
        try {
            const result: ResponseModel[] = [];
            let data = await this.httpRequest(result, [url], true);
            if (data.links && data.links.length > 0) {
                data = await this.httpRequest(result, data.links, false);
            }
            
            return {
                status: true,
                message: "please wait and see logs",
                data: data
            }
        } catch (error: any) {                    
            return { status: false, message: error.message };
        }
    }

    public httpRequest = async (responseData: ResponseModel[], links: string[], flag: boolean): Promise<{responseData: ResponseModel[], links: string[]}> => {
        let linkArr: string[] = [];
        let scraperLinks: string[] = [];
        links.map(link => {
            linkArr.push(link)
            const hostName = encodeURIComponent(link);
            scraperLinks.push(`${mainConfig.SCRAPER_URL}/scraper?token=${mainConfig.SCRAPER_TOKEN}&url=${hostName}&javascript=true`)
        })
        let bodyLinks: string[] = [], data: any[] = [];
        for (const link in scraperLinks) {
            let errorStatusCode: number, errorURL: string;
            const axiosData: any = await axios.get(scraperLinks[link]).catch(err => {
                if (err) {
                    errorStatusCode = err.response.status;
                    errorURL = linkArr[link]
                }
            });
            axiosData ? console.log(axiosData.data.url, axiosData.data.pc_status)
                : console.log(errorURL, errorStatusCode);
            if(axiosData) {
                data.push(axiosData)
            } else {
                data.push({
                    data: {
                        url: errorURL,
                        pc_status: errorStatusCode,
                        body: {}
                    }
                })
            }
        }
        for (const req of data) {
            const res: ResponseModel = {
                _website: flag ? [req.data.url] : [],
                _link: [req.data.url],
                _statusCode: [req.data.pc_status]
            };
            responseData.push(res);
            if (req.data.body && req.data.body.links){ bodyLinks = req.data.body.links };
        }
        return {
            responseData,
            links: bodyLinks
        };
    }
}
export default new ScraperService();