import { Router, Request, Response } from "express";
import APIError from "../../services/APIError";
import { getErrorResponse, getResponse } from "../mainModels";
import Service from './service';


class ScrapRoutes {
    public router = Router();
    constructor() {
        this.routes();
    };
    private routes() {
        this.router.post('/scraping', this.getScraperRespose)
    }

    private getScraperRespose = async (req: Request, res: Response) => {
        try {
            const response = await Service.getInfo(req.body);
            return res.json(response);
        } catch (error) {
            new APIError(error, 500, 'getScraperRespose Error')
            res.status(500).send(getErrorResponse());
        }
    }
}

export default new ScrapRoutes().router;