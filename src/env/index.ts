import * as dotenv from "dotenv";
dotenv.config();

const env: string = process.env.NODE_ENV || 'production';
const mainConfig: IMainConfig = require(`./${env}`).default;

export interface IMainConfig {
    NODE_ENV: string;
    PORT: number;
    ENABLE_CLUSTERS: boolean;
    SCRAPER_URL: string;
    SCRAPER_TOKEN: string
}

export default mainConfig;