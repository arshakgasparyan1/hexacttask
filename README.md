# HexactTask

# git clone https://gitlab.com/arshakgasparyan1/hexacttask.git
# cd hexacttask
# npm install

# npm run dev  (for run nodemoon)

# API
	localhost:3000/scraping
# Postman JSON example Input
{
    "data":[
        {"website": ["http://example1.com/"]},
        {"website": ["https://example2.am/", "http://example3.com/"]}
    ]
}
# Output
[
  {
    _website: [ 'http://example1.com/' ],
    _link: [ 'http://example1.com/' ],
    _statusCode: [ 200 ]
  },
  {
    _website: [],
    _link: [ 'http://www.example1.com/' ],
    _statusCode: [ 200 ]
  },
  {
    _website: [ 'https://example2.am/' ],
    _link: [ 'https://example2.am/' ],
    _statusCode: [ 520 ]
  },
  {
    _website: [ 'http://example3.com/' ],
    _link: [ 'http://example3.com/' ],
    _statusCode: [ 520 ]
  }
]